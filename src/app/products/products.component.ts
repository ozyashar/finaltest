import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'hadardasim-products',
  templateUrl: './products.component.html',
  styles: [`
    .products li { cursor: default; }
    .products li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover,
    .list-group-item.active:focus { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class ProductsComponent implements OnInit {

  isLoading:Boolean = true;
  products;

  currentProduct;

  select(product){
    this.currentProduct = product;
  }


  constructor(private _productsService:ProductsService) { }

 // addUser(user){  //ex 6 class             ex 7 class - deleting this beacuse of fire base
   // this.users.push(user)
 // }

    addProduct(product){                         /// ex 7 class    create this + the one in service for add a new user to fire base
      this._productsService.addProduct(product);
    }
    updateProduct(product){                              // ex 7 class     beafore that there were changes in users.service.ts
      this._productsService.updateProduct(product);          // until now 2 changes to create updateuser
    }

/*                      delete this in ex 7 class
  deleteUser(user){
    this.users.splice(
      this.users.indexOf(user),1
    ) 
}
*/
    deleteProduct(product){                             /// create this in ex 7 class  and after that users.service.ts
      this._productsService.deleteProduct(product);
    }
/*                              delete that in ex 7 class when updating a user
  cancel(user){  // for ex 5
    //this._usersService.updateUser(user); why to use 'usersService' ???
    let user1 = {name:user.name,email:user.email}; 
    // this.users.splice(
    // this.users.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
  // )
    console.log(this.users);
  }
*/

  ngOnInit() {
    this._productsService.getProducts().subscribe(productsData =>
      {this.products = productsData;
        this.isLoading = false
          console.log(this.products)});      // ex 7 class
  }

}
