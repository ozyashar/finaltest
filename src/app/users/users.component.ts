import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'hadardasim-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover,
    .list-group-item.active:focus { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

  isLoading:Boolean = true;
  users;

  currentUser;

  select(user){
    this.currentUser = user;
  }


  constructor(private _usersService:UsersService) { }

 // addUser(user){  //ex 6 class             ex 7 class - deleting this beacuse of fire base
   // this.users.push(user)
 // }

    addUser(user){                         /// ex 7 class    create this + the one in service for add a new user to fire base
      this._usersService.addUser(user);
    }
    updateUser(user){                              // ex 7 class     beafore that there were changes in users.service.ts
      this._usersService.updateUser(user);          // until now 2 changes to create updateuser
    }

/*                      delete this in ex 7 class
  deleteUser(user){
    this.users.splice(
      this.users.indexOf(user),1
    ) 
}
*/
    deleteUser(user){                             /// create this in ex 7 class  and after that users.service.ts
      this._usersService.deleteUser(user);
    }
/*                              delete that in ex 7 class when updating a user
  cancel(user){  // for ex 5
    //this._usersService.updateUser(user); why to use 'usersService' ???
    let user1 = {name:user.name,email:user.email}; 
    // this.users.splice(
    // this.users.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
  // )
    console.log(this.users);
  }
*/

  ngOnInit() {
    this._usersService.getUsers().subscribe(usersData =>
      {this.users = usersData;
        this.isLoading = false
          console.log(this.users)});      // ex 7 class
  }

}
