import { Component, OnInit } from '@angular/core';
import {Customer} from './customer';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
 customer:Customer;
  tempCustomer:Customer = {cost:null,pname:null,categoryId:null,pid:null};  // for ex 5        
  @Output() deleteEvent = new EventEmitter<Customer>();
  @Output() cancelEvent = new EventEmitter<Customer>(); // for ex 5 []
  isEdit:Boolean = false;
  editButtonText = "Edit";
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.customer);
  }
  toggelEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "save" : this.editButtonText = "Edit";

         if(this.isEdit){   /// for ex 5          
       this.tempCustomer.cost = this.customer.cost;
       this.tempCustomer.pname = this.customer.pname;
        this.tempCustomer.pid = this.customer.pid;
     } else {
      // let originalAndNew = [];
      // originalAndNew.push(this.tempUser,this.user);
       this.cancelEvent.emit(this.customer);  // need explanation ####
     } 

  }
  
  onCancel(){ // for ex 5
    this.isEdit = false;
    this.customer.cost = this.tempCustomer.cost;
    this.customer.pname = this.tempCustomer.pname;
    this.customer.pid = this.tempCustomer.pid;
    this.editButtonText = 'Edit';
 

}
 ngOnInit() {
  }
}